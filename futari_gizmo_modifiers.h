/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_gizmo_modifiers.h
 * Author: frankiezafe
 *
 * Created on January 19, 2019, 10:09 AM
 */

#ifndef FUTARI_GIZMO_MODIFIERS_H
#define FUTARI_GIZMO_MODIFIERS_H

#include <iostream>
#include <algorithm>

#include "editor/spatial_editor_gizmos.h"

#include "futari_wind.h"
#include "futari_attractor.h"
#include "futari_vortex.h"
#include "futari_floor.h"

#define FUTARI_CIRCLE_DEFINITION 32

#define FUTARI_DISABLED_COLOR_NAME "editors/3d_gizmos/gizmo_colors/futari_disabled"
#define FUTARI_DISABLED_COLOR 0.0, 0.2, 0.1, 0.6
#define FUTARI_WIND_COLOR_NAME "editors/3d_gizmos/gizmo_colors/futari_wind"
#define FUTARI_WIND_COLOR 0.0, 1.0, 1.0, 1.0
#define FUTARI_ATTRACTOR_COLOR_NAME "editors/3d_gizmos/gizmo_colors/futari_attractor"
#define FUTARI_ATTRACTOR_COLOR 0.125, 1.0, 0.875, 1.0
#define FUTARI_VORTEX_COLOR_NAME "editors/3d_gizmos/gizmo_colors/futari_vortex"
#define FUTARI_VORTEX_COLOR 0.25, 1.0, 0.75, 1.0
#define FUTARI_FLOOR_COLOR_NAME "editors/3d_gizmos/gizmo_colors/futari_floor"
#define FUTARI_FLOOR_COLOR 0.25, 1.0, 0.5, 1.0

#define FUTARI_DISABLED_MATERIAL "futari_disabled"
#define FUTARI_WIND_MATERIAL "futari_wind"
#define FUTARI_ATTRACTOR_MATERIAL "futari_attractor"
#define FUTARI_VORTEX_MATERIAL "futari_vortex"
#define FUTARI_FLOOR_MATERIAL "futari_floor"
#define FUTARI_HANDLE_MATERIAL "futari_handles"


#if defined(__WIN32__) || defined(WIN32) || defined(_WIN32)
// M_PI is not defined on windows... see <math.h> in linux
# define M_PI		3.14159265358979323846	/* pi */
#endif

class FutariModifierGizmoPlugin : public EditorSpatialGizmoPlugin {
    GDCLASS( FutariModifierGizmoPlugin, EditorSpatialGizmoPlugin )

public:

    FutariModifierGizmoPlugin( );

    bool has_gizmo( Spatial *p_spatial );
    String get_name( ) const;
    bool is_selectable_when_hidden( ) const;

    void redraw( EditorSpatialGizmo *p_gizmo );

    String get_handle_name( const EditorSpatialGizmo *p_gizmo, int p_idx ) const;
    Variant get_handle_value( EditorSpatialGizmo *p_gizmo, int p_idx ) const;
    void set_handle( EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point );
    void commit_handle( EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel );

    static void curved_arrow(
                       const real_t radius,
                       const real_t length,
                       const real_t thickness,
                       const real_t radian,
                       Vector<Vector3>& arr );
    
    static void circle( const real_t radius, Vector<Vector3>& c );
    
protected:

};

class FutariWindGizmoPlugin : public FutariModifierGizmoPlugin {
    GDCLASS( FutariWindGizmoPlugin, FutariModifierGizmoPlugin )

public:

    FutariWindGizmoPlugin( );

    void init_materials( );
    bool has_gizmo( Spatial *p_spatial );
    String get_name( ) const;

    void redraw( EditorSpatialGizmo *p_gizmo );

};

class FutariAttractorGizmoPlugin : public FutariModifierGizmoPlugin {
    GDCLASS( FutariAttractorGizmoPlugin, FutariModifierGizmoPlugin )

public:

    FutariAttractorGizmoPlugin( );

    void init_materials( );
    bool has_gizmo( Spatial *p_spatial );
    String get_name( ) const;

    void redraw( EditorSpatialGizmo *p_gizmo );

};

class FutariVortexGizmoPlugin : public FutariModifierGizmoPlugin {
    GDCLASS( FutariVortexGizmoPlugin, FutariModifierGizmoPlugin )

public:

    FutariVortexGizmoPlugin( );

    void init_materials( );
    bool has_gizmo( Spatial *p_spatial );
    String get_name( ) const;

    void redraw( EditorSpatialGizmo *p_gizmo );

};

class FutariFloorGizmoPlugin : public FutariModifierGizmoPlugin {
    GDCLASS( FutariFloorGizmoPlugin, FutariModifierGizmoPlugin )

public:

    FutariFloorGizmoPlugin( );

    void init_materials( );
    bool has_gizmo( Spatial *p_spatial );
    String get_name( ) const;

    void redraw( EditorSpatialGizmo *p_gizmo );

};

#endif /* FUTARI_GIZMO_MODIFIERS_H */