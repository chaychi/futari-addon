/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_editor_plugin.cpp
 * Author: frankiezafe
 * 
 * Created on January 9, 2019, 11:36 AM
 */

#include "futari_editor_plugin.h"

EditorPluginFutari::EditorPluginFutari(EditorNode *p_editor) {

    Ref<FutariWindGizmoPlugin> futari_wind_plugin = Ref<FutariWindGizmoPlugin>(memnew(FutariWindGizmoPlugin));
    SpatialEditor::get_singleton()->add_gizmo_plugin(futari_wind_plugin);

    Ref<FutariAttractorGizmoPlugin> futari_attractor_plugin = Ref<FutariAttractorGizmoPlugin>(memnew(FutariAttractorGizmoPlugin));
    SpatialEditor::get_singleton()->add_gizmo_plugin(futari_attractor_plugin);

    Ref<FutariVortexGizmoPlugin> futari_vortex_plugin = Ref<FutariVortexGizmoPlugin>(memnew(FutariVortexGizmoPlugin));
    SpatialEditor::get_singleton()->add_gizmo_plugin(futari_vortex_plugin);

    Ref<FutariFloorGizmoPlugin> futari_floor_plugin = Ref<FutariFloorGizmoPlugin>(memnew(FutariFloorGizmoPlugin));
    SpatialEditor::get_singleton()->add_gizmo_plugin(futari_floor_plugin);

    Ref<FutariParticlesGizmoPlugin> futari_particles_plugin = Ref<FutariParticlesGizmoPlugin>(memnew(FutariParticlesGizmoPlugin));
    SpatialEditor::get_singleton()->add_gizmo_plugin(futari_particles_plugin);

}