/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_particles.cpp
 * Author: frankiezafe
 * 
 * Created on January 6, 2019, 7:26 PM
 */

#include "futari_particles.h"

FutariParticles::FutariParticles() :
bypass_last_revision(true),
last_revision(0),
_procmat_refresh(false),
futari_layers(1) {

    particles = VS::get_singleton()->particles_create();
    set_base(particles);
    set_emitting(true);
    set_one_shot(false);
    set_amount(8);
    set_lifetime(1);
    set_fixed_fps(0);
    set_fractional_delta(true);
    set_pre_process_time(0);
    set_explosiveness_ratio(0);
    set_randomness_ratio(0);
    set_visibility_aabb(AABB(Vector3(-4, -4, -4), Vector3(8, 8, 8)));
    set_use_local_coordinates(false);
    set_draw_passes(1);
    set_draw_order(FDRAW_ORDER_INDEX);
    set_speed_scale(1);
    
}

FutariParticles::~FutariParticles() {

    VS::get_singleton()->free(particles);

}

void FutariParticles::set_futari_layer(uint32_t p_mask) {
    futari_layers = p_mask;
}

uint32_t FutariParticles::get_futari_layer() const {
    return futari_layers;
}

void FutariParticles::set_futari_layer_bit(int p_layer, bool p_enable) {
    ERR_FAIL_INDEX(p_layer, 32);
    if (p_enable) {
        set_futari_layer(futari_layers | (1 << p_layer));
    } else {
        set_futari_layer(futari_layers & (~(1 << p_layer)));
    }
}

bool FutariParticles::get_futari_layer_bit(int p_layer) const {
    ERR_FAIL_INDEX_V(p_layer, 32, false);
    return (futari_layers & (1 << p_layer));
}

void FutariParticles::_bind_methods() {

    ClassDB::bind_method("_internal_process", &FutariParticles::_internal_process);

    ClassDB::bind_method(D_METHOD("set_emitting", "emitting"), &FutariParticles::set_emitting);
    ClassDB::bind_method(D_METHOD("set_amount", "amount"), &FutariParticles::set_amount);
    ClassDB::bind_method(D_METHOD("set_lifetime", "secs"), &FutariParticles::set_lifetime);
    ClassDB::bind_method(D_METHOD("set_one_shot", "enable"), &FutariParticles::set_one_shot);
    ClassDB::bind_method(D_METHOD("set_pre_process_time", "secs"), &FutariParticles::set_pre_process_time);
    ClassDB::bind_method(D_METHOD("set_explosiveness_ratio", "ratio"), &FutariParticles::set_explosiveness_ratio);
    ClassDB::bind_method(D_METHOD("set_randomness_ratio", "ratio"), &FutariParticles::set_randomness_ratio);
    ClassDB::bind_method(D_METHOD("set_visibility_aabb", "aabb"), &FutariParticles::set_visibility_aabb);
    ClassDB::bind_method(D_METHOD("set_use_local_coordinates", "enable"), &FutariParticles::set_use_local_coordinates);
    ClassDB::bind_method(D_METHOD("set_fixed_fps", "fps"), &FutariParticles::set_fixed_fps);
    ClassDB::bind_method(D_METHOD("set_fractional_delta", "enable"), &FutariParticles::set_fractional_delta);
    ClassDB::bind_method(D_METHOD("set_process_material", "material"), &FutariParticles::set_process_material);
    ClassDB::bind_method(D_METHOD("set_speed_scale", "scale"), &FutariParticles::set_speed_scale);

    ClassDB::bind_method(D_METHOD("is_emitting"), &FutariParticles::is_emitting);
    ClassDB::bind_method(D_METHOD("get_amount"), &FutariParticles::get_amount);
    ClassDB::bind_method(D_METHOD("get_lifetime"), &FutariParticles::get_lifetime);
    ClassDB::bind_method(D_METHOD("get_one_shot"), &FutariParticles::get_one_shot);
    ClassDB::bind_method(D_METHOD("get_pre_process_time"), &FutariParticles::get_pre_process_time);
    ClassDB::bind_method(D_METHOD("get_explosiveness_ratio"), &FutariParticles::get_explosiveness_ratio);
    ClassDB::bind_method(D_METHOD("get_randomness_ratio"), &FutariParticles::get_randomness_ratio);
    ClassDB::bind_method(D_METHOD("get_visibility_aabb"), &FutariParticles::get_visibility_aabb);
    ClassDB::bind_method(D_METHOD("get_use_local_coordinates"), &FutariParticles::get_use_local_coordinates);
    ClassDB::bind_method(D_METHOD("get_fixed_fps"), &FutariParticles::get_fixed_fps);
    ClassDB::bind_method(D_METHOD("get_fractional_delta"), &FutariParticles::get_fractional_delta);
    ClassDB::bind_method(D_METHOD("get_process_material"), &FutariParticles::get_process_material);
    ClassDB::bind_method(D_METHOD("get_speed_scale"), &FutariParticles::get_speed_scale);

    ClassDB::bind_method(D_METHOD("set_draw_order", "order"), &FutariParticles::set_draw_order);
    ClassDB::bind_method(D_METHOD("get_draw_order"), &FutariParticles::get_draw_order);

    ClassDB::bind_method(D_METHOD("set_draw_passes", "passes"), &FutariParticles::set_draw_passes);
    ClassDB::bind_method(D_METHOD("set_draw_pass_mesh", "pass", "mesh"), &FutariParticles::set_draw_pass_mesh);

    ClassDB::bind_method(D_METHOD("get_draw_passes"), &FutariParticles::get_draw_passes);
    ClassDB::bind_method(D_METHOD("get_draw_pass_mesh", "pass"), &FutariParticles::get_draw_pass_mesh);

    ClassDB::bind_method(D_METHOD("restart"), &FutariParticles::restart);
    ClassDB::bind_method(D_METHOD("capture_aabb"), &FutariParticles::capture_aabb);

    ClassDB::bind_method(D_METHOD("set_futari_layer", "futari_mask"), &FutariParticles::set_futari_layer);
    ClassDB::bind_method(D_METHOD("get_futari_layer"), &FutariParticles::get_futari_layer);
    ClassDB::bind_method(D_METHOD("set_futari_layer_bit", "futari_layer", "enabled"), &FutariParticles::set_futari_layer_bit);
    ClassDB::bind_method(D_METHOD("get_futari_layer_bit", "futari_layer"), &FutariParticles::get_futari_layer_bit);

    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "emitting"), "set_emitting", "is_emitting");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "amount", PROPERTY_HINT_EXP_RANGE, "1,1000000,1"), "set_amount", "get_amount");

    ADD_GROUP("Time", "");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "lifetime", PROPERTY_HINT_EXP_RANGE, "0.01,600.0,0.01"), "set_lifetime", "get_lifetime");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "one_shot"), "set_one_shot", "get_one_shot");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "preprocess", PROPERTY_HINT_EXP_RANGE, "0.00,600.0,0.01"), "set_pre_process_time", "get_pre_process_time");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "speed_scale", PROPERTY_HINT_RANGE, "0,64,0.01"), "set_speed_scale", "get_speed_scale");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "explosiveness", PROPERTY_HINT_RANGE, "0,1,0.01"), "set_explosiveness_ratio", "get_explosiveness_ratio");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "randomness", PROPERTY_HINT_RANGE, "0,1,0.01"), "set_randomness_ratio", "get_randomness_ratio");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "fixed_fps", PROPERTY_HINT_RANGE, "0,1000,1"), "set_fixed_fps", "get_fixed_fps");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "fract_delta"), "set_fractional_delta", "get_fractional_delta");

    ADD_GROUP("Drawing", "");
    ADD_PROPERTY(PropertyInfo(Variant::AABB, "visibility_aabb"), "set_visibility_aabb", "get_visibility_aabb");
    //    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "local_coords"), "set_use_local_coordinates", "get_use_local_coordinates");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "draw_order", PROPERTY_HINT_ENUM, "Index,Lifetime,View Depth"), "set_draw_order", "get_draw_order");

    ADD_GROUP("Process Material", "");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "process_material", PROPERTY_HINT_RESOURCE_TYPE, "ShaderMaterial,FutariMaterial"), "set_process_material", "get_process_material");

    ADD_GROUP("Draw Passes", "draw_");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "draw_passes", PROPERTY_HINT_RANGE, "0," + itos(FMAX_DRAW_PASSES) + ",1"), "set_draw_passes", "get_draw_passes");
    for (int i = 0; i < FMAX_DRAW_PASSES; i++) {
        ADD_PROPERTYI(PropertyInfo(Variant::OBJECT, "draw_pass_" + itos(i + 1), PROPERTY_HINT_RESOURCE_TYPE, "Mesh"), "set_draw_pass_mesh", "get_draw_pass_mesh", i);
    }

    ADD_GROUP("Layers", "");
    ADD_PROPERTY(PropertyInfo(Variant::INT, "futari_layers", PROPERTY_HINT_LAYERS_3D_RENDER), "set_futari_layer", "get_futari_layer");

    BIND_ENUM_CONSTANT(FDRAW_ORDER_INDEX);
    BIND_ENUM_CONSTANT(FDRAW_ORDER_LIFETIME);
    BIND_ENUM_CONSTANT(FDRAW_ORDER_VIEW_DEPTH);

    BIND_CONSTANT(FMAX_DRAW_PASSES);

}

void FutariParticles::_clear_modifier_vectors() {

    _modifiers.clear();
    _attractors.clear();
    _winds.clear();
    _vortices.clear();
    _floors.clear();

}

bool FutariParticles::_is_empty_modifier_vectors() {

    return _attractors.empty() &&
            _winds.empty() &&
            _vortices.empty() &&
            _floors.empty();

}

void FutariParticles::_rebuild_modifier_vectors() {

    std::vector< FutariModifierData* >& ds = FutariServer::data();
    std::vector< FutariModifierData* >::iterator it;
    std::vector< FutariModifierData* >::iterator ite = ds.end();

    _clear_modifier_vectors();

    for (it = ds.begin(); it != ite; ++it) {

        FutariModifierData* fmd = (*it);

        if (futari_layers & fmd->futari_layers) {

            _modifiers.push_back(fmd);

            switch (fmd->futari_type) {

                case FutariModifierData::FMT_ATTRACTOR:
                    _attractors.push_back((FutariAttractorData*) fmd);
                    break;
                case FutariModifierData::FMT_WIND:
                    _winds.push_back((FutariWindData*) fmd);
                    break;
                case FutariModifierData::FMT_VORTEX:
                    _vortices.push_back((FutariVortexData*) fmd);
                    break;
                case FutariModifierData::FMT_FLOOR:
                    _floors.push_back((FutariFloorData*) fmd);
                    break;

            }
        }

    }

}

bool FutariParticles::_validate_modifier_vectors() {

    int size = _modifiers.size();
    for (int i = 0; i < size; ++i) {
        if (!(futari_layers & _modifiers[i]->futari_layers)) {
            std::cout << "one of the modifier just changed layer and NOT affects particles anymore!" << std::endl;
            _rebuild_modifier_vectors();
            _procmat_refresh = true;
            return false;
        }
    }

    std::vector< FutariModifierData* >& ds = FutariServer::data();
    std::vector< FutariModifierData* >::iterator it;
    std::vector< FutariModifierData* >::iterator ite = ds.end();

    for (it = ds.begin(); it != ite; ++it) {

        FutariModifierData* fmd = (*it);
        if (futari_layers & fmd->futari_layers && _modifiers.find( fmd ) == -1 ) {
            std::cout << "one of the modifier just changed layer and starts affecting particles NOW!" << std::endl;
            std::cout << "_modifiers: " << _modifiers.size() << std::endl;
            std::cout << "_attractors: " << _attractors.size() << std::endl;
            std::cout << "_winds: " << _winds.size() << std::endl;
            std::cout << "_vortices: " << _vortices.size() << std::endl;
            std::cout << "_floors: " << _floors.size() << std::endl;
            _rebuild_modifier_vectors();
            _procmat_refresh = true;
            return false;
        }
        
    }

    return true;

}

bool FutariParticles::_update_modifier_vectors() {

    FutariServer::lock();

    if (bypass_last_revision || last_revision != FutariServer::revision()) {

        bypass_last_revision = false;
        last_revision = FutariServer::revision();

        // something important have changed in the scene!
        // modifiers have appeared or disappeared, or it is the first time
        // these particles updates => we have to rebuild the vectors
        // of modifiers and trigger a shader recompilation
        _rebuild_modifier_vectors();
        _procmat_refresh = true;
        FutariServer::unlock();
        return true;

    }

    // checking layers first
    if (!_validate_modifier_vectors()) {
        // one of the modifier changed layer and does not affects
        // particles anymore -> full recompilation
        FutariServer::unlock();
        return true;
    }

    // standard update, we can now use _modifiers instead of 
    // accessing to all modifiers stored in the server

    bool smth_changed = false;

    int size = _modifiers.size();
    for (int i = 0; i < size; ++i) {

        FutariModifierData* fmd = _modifiers[i];

        if (fmd->changed) {
            smth_changed = true;
        }

        if (
                fmd->changed_strength_decay ||
                fmd->changed_lifetime ||
                fmd->changed_enabled
                ) {
            _procmat_refresh = true;
        }

        if (!fmd->enabled) {
            continue;
        }

        switch (fmd->futari_type) {

            case FutariModifierData::FMT_ATTRACTOR:
                break;

            case FutariModifierData::FMT_FLOOR:
            {
                FutariFloorData* ffd = (FutariFloorData*) fmd;
                if (ffd->changed_infinite) {
                    _procmat_refresh = true;
                }
            }
                break;

            case FutariModifierData::FMT_WIND:
                break;

            case FutariModifierData::FMT_VORTEX:
            {
                FutariVortexData* fvd = (FutariVortexData*) fmd;
                if (fvd->changed_top_tex || fvd->changed_bottom_tex || fvd->changed_height_tex) {
                    _procmat_refresh = true;
                }
            }
                break;

            default:
                break;

        }

    }

    FutariServer::unlock();
    return smth_changed;

}

void FutariParticles::_regenerate_attractors(Ref<FutariMaterial>& procmat) {

    FutariShaderPart fs_part;

    int size = _attractors.size();
    for (int id = 0; id < size; ++id) {

        if (id == 0) {
            fs_part.ss_declaration << "\n//////// FUTARI ATTRACTOR -- start\n";
            fs_part.ss_vertex_normal << "\n//////// FUTARI ATTRACTOR -- start\n";
        }

        FutariAttractorData* fad = _attractors[id];

        if (fad->enabled) {

            fs_part.ss_declaration << "uniform vec3 attractor_position_" << id << ";\n";
            fs_part.ss_declaration << "uniform float attractor_strength_" << id << ";\n";
            fs_part.ss_declaration << "uniform float attractor_range_" << id << ";\n";
            fs_part.ss_declaration << "uniform float attractor_vmult_" << id << ";\n";
            fs_part.ss_declaration << "uniform float attractor_pmult_" << id << ";\n";
            if (fad->strength_decay.is_valid()) {
                fs_part.ss_declaration << "uniform sampler2D attractor_strength_decay_" << id << " : hint_white;\n";
            }
            if (fad->lifetime.is_valid()) {
                fs_part.ss_declaration << "uniform sampler2D attractor_lifetime_" << id << " : hint_white;\n";
            }

            fs_part.ss_vertex_normal << "fut_v3 = attractor_position_" << id << " - pos;\n";
            fs_part.ss_vertex_normal << "fut_d = 1.0 - min( 1.0, (fut_v3.x*fut_v3.x + fut_v3.y*fut_v3.y + fut_v3.z*fut_v3.z) / attractor_range_" << id << " );\n";
            if (fad->strength_decay.is_valid()) {
                fs_part.ss_vertex_normal << "fut_d = textureLod(attractor_strength_decay_" << id << ", vec2( fut_d, 0.0 ), 0.0).r;\n";
            }
            fs_part.ss_vertex_normal << "fut_v3 = normalize( fut_v3 ) * attractor_strength_" << id << " * fut_d;\n";
            if (fad->lifetime.is_valid()) {
                fs_part.ss_vertex_normal << "fut_v3 = fut_v3 * textureLod(attractor_lifetime_" << id << ", vec2( CUSTOM.y, 0.0 ), 0.0).r;\n";
            }
            fs_part.ss_vertex_normal << "futari_force += fut_v3 * attractor_vmult_" << id << ";\n";
            fs_part.ss_vertex_normal << "futari_push += fut_v3 * attractor_pmult_" << id << ";\n";

            ++fs_part.modifier_count;

        }

        if (id == size - 1) {
            fs_part.ss_declaration << "\n//////// FUTARI ATTRACTOR -- end\n";
            fs_part.ss_vertex_normal << "\n//////// FUTARI ATTRACTOR -- end\n";
        }

    }

    fs_part.pack();
    procmat->set_attractor_shaderpart(fs_part);

}

void FutariParticles::_update_attractors_param(Ref<FutariMaterial>& procmat) {

    int mnum = _attractors.size();
    for (int i = 0; i < mnum; ++i) {

        FutariAttractorData* fad = _attractors[i];

        if (!_procmat_refresh && !fad->changed) {
            continue;
        }

        String suffix = String::num(i);

        if (_procmat_refresh) {

            procmat->set_futari_param_v3("attractor_position_" + suffix, fad->position);
            procmat->set_futari_param_real("attractor_strength_" + suffix, fad->strength);
            procmat->set_futari_param_real("attractor_range_" + suffix, fad->range * fad->range);
            procmat->set_futari_param_real("attractor_vmult_" + suffix, fad->velocity_mult);
            procmat->set_futari_param_real("attractor_pmult_" + suffix, fad->position_mult);
            if (fad->strength_decay.is_valid()) {
                procmat->set_futari_param_tex("attractor_strength_decay_" + suffix, fad->strength_decay);
            }
            if (fad->lifetime.is_valid()) {
                procmat->set_futari_param_tex("attractor_lifetime_" + suffix, fad->lifetime);
            }

        } else if (fad->changed) {

            if (fad->changed_position) {
                procmat->set_futari_param_v3("attractor_position_" + suffix, fad->position);
            }
            if (fad->changed_strength) {
                procmat->set_futari_param_real("attractor_strength_" + suffix, fad->strength);
            }
            if (fad->changed_range) {
                procmat->set_futari_param_real("attractor_range_" + suffix, fad->range * fad->range);
            }
            if (fad->changed_velocity_mult) {
                procmat->set_futari_param_real("attractor_vmult_" + suffix, fad->velocity_mult);
            }
            if (fad->changed_position_mult) {
                procmat->set_futari_param_real("attractor_pmult_" + suffix, fad->position_mult);
            }

        }

    }

}

void FutariParticles::_regenerate_winds(Ref<FutariMaterial>& procmat) {

    FutariShaderPart fs_part;

    int size = _winds.size();
    for (int id = 0; id < size; ++id) {

        if (id == 0) {
            fs_part.ss_declaration << "\n//////// FUTARI WIND -- start\n";
            fs_part.ss_vertex_normal << "\n//////// FUTARI WIND -- start\n";
        }

        FutariWindData* fwd = _winds[id];

        if (fwd->enabled) {

            fs_part.ss_declaration << "uniform vec3 wind_position_" << id << ";\n";
            fs_part.ss_declaration << "uniform vec3 wind_orientation_" << id << ";\n";
            fs_part.ss_declaration << "uniform float wind_strength_" << id << ";\n";
            fs_part.ss_declaration << "uniform float wind_range_" << id << ";\n";
            fs_part.ss_declaration << "uniform float wind_vmult_" << id << ";\n";
            fs_part.ss_declaration << "uniform float wind_pmult_" << id << ";\n";
            if (fwd->strength_decay.is_valid()) {
                fs_part.ss_declaration << "uniform sampler2D wind_strength_decay_" << id << " : hint_white;\n";
            }
            if (fwd->lifetime.is_valid()) {
                fs_part.ss_declaration << "uniform sampler2D wind_lifetime_" << id << " : hint_white;\n";
            }

            fs_part.ss_vertex_normal << "fut_v3 = pos - wind_position_" << id << ";\n";
            fs_part.ss_vertex_normal << "fut_d = 1.0 - min( 1.0, (fut_v3.x*fut_v3.x + fut_v3.y*fut_v3.y + fut_v3.z*fut_v3.z) / wind_range_" << id << " );\n";
            if (fwd->strength_decay.is_valid()) {
                fs_part.ss_vertex_normal << "fut_d = textureLod(wind_strength_decay_" << id << ", vec2( fut_d, 0.0 ), 0.0).r;\n";
            }
            fs_part.ss_vertex_normal << "fut_v3 = wind_orientation_" << id << " * wind_strength_" << id << " * fut_d;\n";
            if (fwd->lifetime.is_valid()) {
                fs_part.ss_vertex_normal << "fut_v3 = fut_v3 * textureLod(wind_lifetime_" << id << ", vec2( CUSTOM.y, 0.0 ), 0.0).r;\n";
            }
            fs_part.ss_vertex_normal << "futari_force += fut_v3 * wind_vmult_" << id << ";\n";
            fs_part.ss_vertex_normal << "futari_push += fut_v3 * wind_pmult_" << id << ";\n";

            ++fs_part.modifier_count;

        }

        if (id == size - 1) {
            fs_part.ss_declaration << "\n//////// FUTARI WIND -- end\n";
            fs_part.ss_vertex_normal << "\n//////// FUTARI WIND -- end\n";
        }

    }

    fs_part.pack();
    procmat->set_wind_shaderpart(fs_part);

}

void FutariParticles::_update_winds_param(Ref<FutariMaterial>& procmat) {

    int mnum = _winds.size();
    for (int i = 0; i < mnum; ++i) {

        FutariWindData* fwd = _winds[i];

        if (!_procmat_refresh && !fwd->changed) {
            continue;
        }

        String suffix = String::num(i);

        if (_procmat_refresh) {

            procmat->set_futari_param_v3("wind_position_" + suffix, fwd->position);
            procmat->set_futari_param_v3("wind_orientation_" + suffix, fwd->orientation);
            procmat->set_futari_param_real("wind_strength_" + suffix, fwd->strength);
            procmat->set_futari_param_real("wind_range_" + suffix, fwd->range * fwd->range);
            procmat->set_futari_param_real("wind_vmult_" + suffix, fwd->velocity_mult);
            procmat->set_futari_param_real("wind_pmult_" + suffix, fwd->position_mult);
            if (fwd->strength_decay.is_valid()) {
                procmat->set_futari_param_tex("wind_strength_decay_" + suffix, fwd->strength_decay);
            }
            if (fwd->lifetime.is_valid()) {
                procmat->set_futari_param_tex("wind_lifetime_" + suffix, fwd->lifetime);
            }

        } else if (fwd->changed) {

            if (fwd->changed_position) {
                procmat->set_futari_param_v3("wind_position_" + suffix, fwd->position);
            }
            if (fwd->changed_orientation) {
                procmat->set_futari_param_v3("wind_orientation_" + suffix, fwd->orientation);
            }
            if (fwd->changed_strength) {
                procmat->set_futari_param_real("wind_strength_" + suffix, fwd->strength);
            }
            if (fwd->changed_range) {
                procmat->set_futari_param_real("wind_range_" + suffix, fwd->range * fwd->range);
            }
            if (fwd->changed_velocity_mult) {
                procmat->set_futari_param_real("wind_vmult_" + suffix, fwd->velocity_mult);
            }
            if (fwd->changed_position_mult) {
                procmat->set_futari_param_real("wind_pmult_" + suffix, fwd->position_mult);
            }

        }

    }

}

void FutariParticles::_regenerate_vortices(Ref<FutariMaterial>& procmat) {

    FutariShaderPart fs_part;

    int size = _vortices.size();
    for (int id = 0; id < size; ++id) {

        if (id == 0) {
            fs_part.ss_declaration << "\n//////// FUTARI VORTEX -- start\n";
            fs_part.ss_vertex_normal << "\n//////// FUTARI VORTEX -- start\n";
        }

        FutariVortexData* fvd = _vortices[id];

        if (fvd->enabled) {

            fs_part.ss_declaration << "uniform vec3 vortex_position_" << id << ";\n";
            fs_part.ss_declaration << "uniform vec3 vortex_orientation_" << id << ";\n";
            fs_part.ss_declaration << "uniform float vortex_strength_" << id << ";\n";
            fs_part.ss_declaration << "uniform float vortex_range_" << id << ";\n";
            fs_part.ss_declaration << "uniform float vortex_height_" << id << ";\n";
            fs_part.ss_declaration << "uniform float vortex_vmult_" << id << ";\n";
            fs_part.ss_declaration << "uniform float vortex_pmult_" << id << ";\n";

            if (fvd->strength_decay.is_valid()) {

                fs_part.ss_declaration << "uniform sampler2D vortex_gradient_center_" << id << " : hint_white;\n";

                if (fvd->top_tex.is_valid() && fvd->bottom_tex.is_valid() && fvd->height_tex.is_valid()) {
                    // activation of 3 gradient vortex
                    fs_part.ss_declaration << "uniform sampler2D vortex_gradient_top_" << id << " : hint_white;\n";
                    fs_part.ss_declaration << "uniform sampler2D vortex_gradient_bottom_" << id << " : hint_white;\n";
                }

            }
            if (fvd->height_tex.is_valid()) {
                fs_part.ss_declaration << "uniform sampler2D vortex_gradient_height_" << id << " : hint_white;\n";
            }
            if (fvd->lifetime.is_valid()) {
                fs_part.ss_declaration << "uniform sampler2D vortex_lifetime_" << id << " : hint_white;\n";
            }

            // vortex is a cylinder, therefore we need to compute 2 ratio:
            // - position on height, between -1 & 1
            // - distance to center of basis, between 0 and 1
            // relative position of particle
            fs_part.ss_vertex_normal << "fut_v3 = pos - vortex_position_" << id << ";\n";
            // normalised projection on vortex Y axis
            fs_part.ss_vertex_normal << "fut_v3_norm = fut_v3 / vortex_height_" << id << ";\n";
            fs_part.ss_vertex_normal << "fut_h = max( -1, min( 1, dot( fut_v3_norm, vortex_orientation_" << id << " )));\n";
            // computation of perpendicular vector between vortex Y axis and particle
            fs_part.ss_vertex_normal << "fut_v3 = pos - ( vortex_position_" << id << " + fut_h * vortex_height_" << id << " * vortex_orientation_" << id << " );\n";
            // distance to basis center
            fs_part.ss_vertex_normal << "fut_d = 1.0 - min( 1.0, (fut_v3.x*fut_v3.x + fut_v3.y*fut_v3.y + fut_v3.z*fut_v3.z) / vortex_range_" << id << " );\n";
            // normalisation of perpendicular (X axis)
            fs_part.ss_vertex_normal << "fut_v3_norm = normalize( fut_v3 );\n";
            // computation of tangent (Z axis)
            fs_part.ss_vertex_normal << "fut_v3_tan = cross( fut_v3_norm, vortex_orientation_" << id << " );\n";
            // everything is ready to use!
            // to resume:
            // - fut_h, percentage of the height
            // - fut_d, percentage of radius
            // - [X axis] fut_v3_norm > represents the centrifuge direction
            // - [Y axis] vortex_orientation_* > represents the up direction
            // - [Z axis] fut_v3_tan > represents the tangential direction
            // let's apply this!
            if (fvd->strength_decay.is_valid()) {

                if (fvd->top_tex.is_valid() && fvd->bottom_tex.is_valid() && fvd->height_tex.is_valid()) {

                    // activation of 3 gradient vortex
                    fs_part.ss_vertex_normal << "fut_v3_top = ( vec3(-0.5,-0.5,-0.5) + textureLod(vortex_gradient_top_" << id << ", vec2( fut_d, 0.0 ), 0.0).xyz ) * 2.0;\n";
                    fs_part.ss_vertex_normal << "fut_v3_center = ( vec3(-0.5,-0.5,-0.5) + textureLod(vortex_gradient_center_" << id << ", vec2( fut_d, 0.0 ), 0.0).xyz ) * 2.0;\n";
                    fs_part.ss_vertex_normal << "fut_v3_bottom = ( vec3(-0.5,-0.5,-0.5) + textureLod(vortex_gradient_bottom_" << id << ", vec2( fut_d, 0.0 ), 0.0).xyz ) * 2.0;\n";

                    fs_part.ss_vertex_normal << "fut_v3_h = textureLod(vortex_gradient_height_" << id << ", vec2( (fut_h+1.0) * 0.5, 0.0 ), 0.0).xyz;\n";

                    // blending influences of the 3 channels of the the top, center & bottom gradients
                    fs_part.ss_vertex_normal << "fut_v3.x = fut_v3_h.x * fut_v3_top.x + fut_v3_h.y * fut_v3_center.x + fut_v3_h.z * fut_v3_bottom.x;\n";
                    fs_part.ss_vertex_normal << "fut_v3.y = fut_v3_h.x * fut_v3_top.y + fut_v3_h.y * fut_v3_center.y + fut_v3_h.z * fut_v3_bottom.y;\n";
                    fs_part.ss_vertex_normal << "fut_v3.z = fut_v3_h.x * fut_v3_top.z + fut_v3_h.y * fut_v3_center.z + fut_v3_h.z * fut_v3_bottom.z;\n";
                    //                            fs_part.ss_vertex_normal << "futari_force += fut_v3_tan * (fut_h+1.0) * 0.5 * vortex_strength_" << id << ";\n";

                    // blending of 3 top, center & bottom gradients depending on height gradient
                    fs_part.ss_vertex_normal << "fut_v3_total = fut_v3.x * fut_v3_norm * vortex_strength_" << id << ";\n";
                    fs_part.ss_vertex_normal << "fut_v3_total += fut_v3.y * vortex_orientation_" << id << " * vortex_strength_" << id << ";\n";
                    fs_part.ss_vertex_normal << "fut_v3_total += fut_v3.z * fut_v3_tan * vortex_strength_" << id << ";\n";

                } else {

                    // activation of the one gradient vortex
                    fs_part.ss_vertex_normal << "fut_v3_center = ( vec3(-0.5,-0.5,-0.5) + textureLod(vortex_gradient_center_" << id << ", vec2( fut_d, 0.0 ), 0.0).xyz ) * 2.0;\n";
                    fs_part.ss_vertex_normal << "fut_v3_total = fut_v3_center.x * vortex_strength_" << id << " * fut_v3_norm;\n";
                    fs_part.ss_vertex_normal << "fut_v3_total += fut_v3_center.y * vortex_strength_" << id << " * vortex_orientation_" << id << ";\n";
                    fs_part.ss_vertex_normal << "fut_v3_total += fut_v3_center.z * vortex_strength_" << id << " * fut_v3_tan;\n";

                }

            } else {

                if (fvd->height_tex.is_valid()) {
                    fs_part.ss_vertex_normal << "fut_v3_h = textureLod(vortex_gradient_height_" << id << ", vec2( (fut_h+1.0) * 0.5, 0.0 ), 0.0).xyz;\n";
                    fs_part.ss_vertex_normal << "fut_v3_total = fut_v3_h.x * fut_d * vortex_strength_" << id << " * fut_v3_tan;\n";
                } else {
                    fs_part.ss_vertex_normal << "fut_v3_total = fut_d * vortex_strength_" << id << " * fut_v3_tan;\n";
                }

            }

            if (fvd->lifetime.is_valid()) {
                fs_part.ss_vertex_normal << "fut_v3_total = fut_v3_total * textureLod(vortex_lifetime_" << id << ", vec2( CUSTOM.y, 0.0 ), 0.0).r;\n";
            }

            fs_part.ss_vertex_normal << "futari_force += fut_v3_total * vortex_vmult_" << id << ";\n";
            fs_part.ss_vertex_normal << "futari_push += fut_v3_total * vortex_pmult_" << id << ";\n";

            //fs_part.ss_vertex_normal << "futari_force += fut_v3_tan * vortex_strength_" << id << " * fut_d;\n";

            ++fs_part.modifier_count;

        }

        if (id == size - 1) {
            fs_part.ss_declaration << "\n//////// FUTARI VORTEX -- end\n";
            fs_part.ss_vertex_normal << "\n//////// FUTARI VORTEX -- end\n";
        }

    }

    fs_part.pack();
    procmat->set_vortex_shaderpart(fs_part);

}

void FutariParticles::_update_vortices_param(Ref<FutariMaterial>& procmat) {

    int mnum = _vortices.size();
    for (int i = 0; i < mnum; ++i) {

        FutariVortexData* fvd = _vortices[i];

        if (!_procmat_refresh && !fvd->changed) {
            continue;
        }

        String suffix = String::num(i);

        if (_procmat_refresh) {

            procmat->set_futari_param_v3("vortex_position_" + suffix, fvd->position);
            procmat->set_futari_param_v3("vortex_orientation_" + suffix, fvd->orientation);
            procmat->set_futari_param_real("vortex_strength_" + suffix, fvd->strength);
            procmat->set_futari_param_real("vortex_range_" + suffix, fvd->range * fvd->range);
            procmat->set_futari_param_real("vortex_height_" + suffix, fvd->height);
            procmat->set_futari_param_real("vortex_vmult_" + suffix, fvd->velocity_mult);
            procmat->set_futari_param_real("vortex_pmult_" + suffix, fvd->position_mult);
            if (fvd->strength_decay.is_valid()) {
                procmat->set_futari_param_tex("vortex_gradient_center_" + suffix, fvd->strength_decay);
                if (fvd->top_tex.is_valid() && fvd->bottom_tex.is_valid() && fvd->height_tex.is_valid()) {
                    procmat->set_futari_param_tex("vortex_gradient_top_" + suffix, fvd->top_tex);
                    procmat->set_futari_param_tex("vortex_gradient_bottom_" + suffix, fvd->bottom_tex);
                }
            }
            if (fvd->height_tex.is_valid()) {
                procmat->set_futari_param_tex("vortex_gradient_height_" + suffix, fvd->height_tex);
            }
            if (fvd->lifetime.is_valid()) {
                procmat->set_futari_param_tex("vortex_lifetime_" + suffix, fvd->lifetime);
            }

        } else if (fvd->changed) {

            if (fvd->changed_position) {
                procmat->set_futari_param_v3("vortex_position_" + suffix, fvd->position);
            }
            if (fvd->changed_orientation) {
                procmat->set_futari_param_v3("vortex_orientation_" + suffix, fvd->orientation);
            }
            if (fvd->changed_strength) {
                procmat->set_futari_param_real("vortex_strength_" + suffix, fvd->strength);
            }
            if (fvd->changed_range) {
                procmat->set_futari_param_real("vortex_range_" + suffix, fvd->range * fvd->range);
            }
            if (fvd->changed_height) {
                procmat->set_futari_param_real("vortex_height_" + suffix, fvd->height);
            }
            if (fvd->changed_velocity_mult) {
                procmat->set_futari_param_real("vortex_vmult_" + suffix, fvd->velocity_mult);
            }
            if (fvd->changed_position_mult) {
                procmat->set_futari_param_real("vortex_pmult_" + suffix, fvd->position_mult);
            }

        }

    }

}

void FutariParticles::_regenerate_floors(Ref<FutariMaterial>& procmat) {

    FutariShaderPart fs_part;

    int size = _floors.size();
    for (int id = 0; id < size; ++id) {

        if (id == 0) {
            fs_part.ss_declaration << "\n//////// FUTARI FLOOR -- start\n";
            fs_part.ss_vertex_normal << "\n//////// FUTARI FLOOR -- start\n";
        }

        FutariFloorData* ffd = _floors[id];

        if (ffd->enabled) {

            fs_part.ss_declaration << "uniform vec3 floor_position_" << id << ";\n";
            fs_part.ss_declaration << "uniform vec3 floor_orientation_" << id << ";\n";
            fs_part.ss_declaration << "uniform vec3 floor_xaxis_" << id << ";\n";
            fs_part.ss_declaration << "uniform vec3 floor_zaxis_" << id << ";\n";
            fs_part.ss_declaration << "uniform float floor_strength_" << id << ";\n";
            fs_part.ss_declaration << "uniform float floor_width_" << id << ";\n";
            fs_part.ss_declaration << "uniform float floor_height_" << id << ";\n";
            fs_part.ss_declaration << "uniform float floor_viscous_" << id << ";\n";
            fs_part.ss_declaration << "uniform float floor_range_" << id << ";\n";
            fs_part.ss_declaration << "uniform float floor_vmult_" << id << ";\n";
            fs_part.ss_declaration << "uniform float floor_pmult_" << id << ";\n";
            if (ffd->strength_decay.is_valid()) {
                fs_part.ss_declaration << "uniform sampler2D floor_strength_decay_" << id << " : hint_white;\n";
            }
            if (ffd->lifetime.is_valid()) {
                fs_part.ss_declaration << "uniform sampler2D floor_lifetime_" << id << " : hint_white;\n";
            }

            fs_part.ss_vertex_normal << "fut_v3 = pos - floor_position_" << id << ";\n";
            if (ffd->infinite) {
                fs_part.ss_vertex_normal << "fut_v3_norm = normalize(fut_v3);";
                fs_part.ss_vertex_normal << "if ( dot( fut_v3_norm, floor_orientation_" << id << " ) < 0.0 ) {";
                // removal of up component in velocity
                fs_part.ss_vertex_normal << "    fut_d = dot(VELOCITY, floor_orientation_" << id << ");\n";
                fs_part.ss_vertex_normal << "    fut_h = 1.0 - abs( abs( dot( fut_v3, floor_orientation_" << id << " ) ) / floor_range_" << id << ");\n";
                fs_part.ss_vertex_normal << "    fut_v3 = ( VELOCITY - fut_d * floor_orientation_" << id << " ) * (1.0 - floor_viscous_" << id << ");\n";
                fs_part.ss_vertex_normal << "    VELOCITY = mix( VELOCITY, fut_v3, fut_h );\n";
                fs_part.ss_vertex_normal << "}";
            } else {
                fs_part.ss_vertex_normal << "fut_xdot = abs( dot( fut_v3, floor_xaxis_" << id << " ) );\n";
                fs_part.ss_vertex_normal << "fut_ydot = abs( dot( fut_v3, floor_orientation_" << id << " ) );\n";
                fs_part.ss_vertex_normal << "fut_zdot = abs( dot( fut_v3, floor_zaxis_" << id << " ) );\n";
                fs_part.ss_vertex_normal << "if ( fut_ydot < floor_range_" << id << " && fut_xdot <= floor_width_" << id << " * 0.5 && fut_zdot <= floor_height_" << id << " * 0.5 ) {\n";
                fs_part.ss_vertex_normal << "    fut_d = dot(VELOCITY, floor_orientation_" << id << ");\n";
                fs_part.ss_vertex_normal << "    fut_h = 1.0 - abs( fut_ydot / floor_range_" << id << ");\n";
                fs_part.ss_vertex_normal << "    fut_v3 = ( VELOCITY - fut_d * floor_orientation_" << id << " ) * (1.0 - floor_viscous_" << id << ");\n";
                fs_part.ss_vertex_normal << "    VELOCITY = mix( VELOCITY, fut_v3, fut_h );\n";
                fs_part.ss_vertex_normal << "    futari_force += fut_h * floor_orientation_" << id << " * floor_strength_" << id << " * floor_vmult_" << id << ";\n";
                fs_part.ss_vertex_normal << "    futari_push += fut_h * floor_orientation_" << id << " * floor_strength_" << id << " * floor_pmult_" << id << ";\n";
                fs_part.ss_vertex_normal << "}\n";
            }
            //                    fs_part.ss_vertex_normal << "fut_d = 1.0 - min( 1.0, (fut_v3.x*fut_v3.x + fut_v3.y*fut_v3.y + fut_v3.z*fut_v3.z) / wind_range_" << id << " );\n";
            //                    if (fmd->strength_decay.is_valid()) {
            //                        fs_part.ss_vertex_normal << "fut_d = textureLod(wind_strength_decay_" << id << ", vec2( fut_d, 0.0 ), 0.0).r;\n";
            //                    }
            //                    fs_part.ss_vertex_normal << "fut_v3 = wind_orientation_" << id << " * wind_strength_" << id << " * fut_d;\n";
            //                    if (fmd->lifetime.is_valid()) {
            //                        fs_part.ss_vertex_normal << "fut_v3 = fut_v3 * textureLod(wind_lifetime_" << id << ", vec2( CUSTOM.y, 0.0 ), 0.0).r;\n";
            //                    }
            //                    fs_part.ss_vertex_normal << "futari_force += fut_v3 * wind_vmult_" << id << ";\n";
            //                    fs_part.ss_vertex_normal << "futari_push += fut_v3 * wind_pmult_" << id << ";\n";

            ++fs_part.modifier_count;

        }

        if (id == size - 1) {
            fs_part.ss_declaration << "\n//////// FUTARI FLOOR -- end\n";
            fs_part.ss_vertex_normal << "\n//////// FUTARI FLOOR -- end\n";
        }

    }

    fs_part.pack();
    procmat->set_floor_shaderpart(fs_part);

}

void FutariParticles::_update_floors_param(Ref<FutariMaterial>& procmat) {

    int mnum = _floors.size();
    for (int i = 0; i < mnum; ++i) {

        FutariFloorData* ffd = _floors[i];

        if (!_procmat_refresh && !ffd->changed) {
            continue;
        }

        String suffix = String::num(i);

        if (_procmat_refresh) {

            procmat->set_futari_param_v3("floor_position_" + suffix, ffd->position);
            procmat->set_futari_param_v3("floor_orientation_" + suffix, ffd->orientation);
            procmat->set_futari_param_v3("floor_xaxis_" + suffix, ffd->xaxis);
            procmat->set_futari_param_v3("floor_zaxis_" + suffix, ffd->zaxis);
            procmat->set_futari_param_real("floor_width_" + suffix, ffd->width);
            procmat->set_futari_param_real("floor_height_" + suffix, ffd->height);
            procmat->set_futari_param_real("floor_viscous_" + suffix, ffd->viscous);
            procmat->set_futari_param_real("floor_strength_" + suffix, ffd->strength);
            procmat->set_futari_param_real("floor_range_" + suffix, ffd->range * ffd->range);
            procmat->set_futari_param_real("floor_vmult_" + suffix, ffd->velocity_mult);
            procmat->set_futari_param_real("floor_pmult_" + suffix, ffd->position_mult);
            if (ffd->strength_decay.is_valid()) {
                procmat->set_futari_param_tex("floor_strength_decay_" + suffix, ffd->strength_decay);
            }
            if (ffd->lifetime.is_valid()) {
                procmat->set_futari_param_tex("floor_lifetime_" + suffix, ffd->lifetime);
            }

        } else if (ffd->changed) {

            if (ffd->changed_position) {
                procmat->set_futari_param_v3("floor_position_" + suffix, ffd->position);
            }
            if (ffd->changed_orientation) {
                procmat->set_futari_param_v3("floor_orientation_" + suffix, ffd->orientation);
            }
            if (ffd->changed_xaxis) {
                procmat->set_futari_param_v3("floor_xaxis_" + suffix, ffd->xaxis);
            }
            if (ffd->changed_zaxis) {
                procmat->set_futari_param_v3("floor_zaxis_" + suffix, ffd->zaxis);
            }
            if (ffd->changed_strength) {
                procmat->set_futari_param_real("floor_width_" + suffix, ffd->width);
            }
            if (ffd->changed_strength) {
                procmat->set_futari_param_real("floor_height_" + suffix, ffd->height);
            }
            if (ffd->changed_width) {
                procmat->set_futari_param_real("floor_width_" + suffix, ffd->width);
            }
            if (ffd->changed_height) {
                procmat->set_futari_param_real("floor_height_" + suffix, ffd->height);
            }
            if (ffd->changed_viscous) {
                procmat->set_futari_param_real("floor_viscous_" + suffix, ffd->viscous);
            }
            if (ffd->changed_strength) {
                procmat->set_futari_param_real("floor_strength_" + suffix, ffd->strength);
            }
            if (ffd->changed_range) {
                procmat->set_futari_param_real("floor_range_" + suffix, ffd->range * ffd->range);
            }
            if (ffd->changed_velocity_mult) {
                procmat->set_futari_param_real("floor_vmult_" + suffix, ffd->velocity_mult);
            }
            if (ffd->changed_position_mult) {
                procmat->set_futari_param_real("floor_pmult_" + suffix, ffd->position_mult);
            }

        }

    }

}

void FutariParticles::_internal_process() {

    // no reason to update shader or anything
    if (
            !is_visible() ||
            !is_visible_in_tree()
            ) {
        return;
    }


    // futari particles can only interact with futari materials
    Ref<Material> mm = get_process_material();
    if (!Object::cast_to<FutariMaterial>(mm.ptr())) {
        return;
    }

    bool _params_refresh = false;

    // if no process material or no flags for modifiers
    // let's save some processing power!
    if (

            process_material.is_null() ||
            (
            !process_material->get_flag(FutariMaterial::FUFLA_ENABLE_WIND) &&
            !process_material->get_flag(FutariMaterial::FUFLA_ENABLE_VORTEX) &&
            !process_material->get_flag(FutariMaterial::FUFLA_ENABLE_ATTRACTOR) &&
            !process_material->get_flag(FutariMaterial::FUFLA_ENABLE_FLOOR)
            )
            ) {

        // return if all modifiers vectors are empty
        if (!_is_empty_modifier_vectors()) {
            // ensure that reenabling modifiers flags will trigger
            // a shader recompilation
            bypass_last_revision = true;
            _clear_modifier_vectors();
            _procmat_refresh = true;
        } else {
            return;
        }

    } else {

        _params_refresh = _update_modifier_vectors();

    }

    if (_procmat_refresh) {
        _params_refresh = true;
    }

    // starting the serious stuff...

    if (_params_refresh) {

        Ref<FutariMaterial> procmat = get_process_material();

        // updating shader if required
        if (_procmat_refresh) {

            FutariShaderPart common_part;
            if (!_is_empty_modifier_vectors()) {

                common_part.modifier_count = _modifiers.size();
                common_part.ss_vertex_normal << "vec3 fut_v3;\n";
                common_part.ss_vertex_normal << "float fut_d;\n";
                common_part.ss_vertex_normal << "float fut_h;\n";
                // for floor only
                common_part.ss_vertex_normal << "float fut_xdot;\n";
                common_part.ss_vertex_normal << "float fut_ydot;\n";
                common_part.ss_vertex_normal << "float fut_zdot;\n";
                // for vortices only
                common_part.ss_vertex_normal << "vec3 fut_v3_total;\n";
                common_part.ss_vertex_normal << "vec3 fut_v3_norm;\n";
                common_part.ss_vertex_normal << "vec3 fut_v3_tan;\n";
                common_part.ss_vertex_normal << "vec3 fut_v3_center;\n";
                common_part.ss_vertex_normal << "vec3 fut_v3_top;\n";
                common_part.ss_vertex_normal << "vec3 fut_v3_bottom;\n";
                common_part.ss_vertex_normal << "vec3 fut_v3_h;\n";

                common_part.pack();
                procmat->set_common_shaderpart(common_part);

                _regenerate_attractors(procmat);
                _regenerate_winds(procmat);
                _regenerate_vortices(procmat);
                _regenerate_floors(procmat);

            } else {

                // full cleanup!
                common_part.pack();
                procmat->set_common_shaderpart(common_part);
                procmat->set_wind_shaderpart(common_part);
                procmat->set_attractor_shaderpart(common_part);
                procmat->set_vortex_shaderpart(common_part);
                procmat->set_floor_shaderpart(common_part);

            }

        }

        // pushing values to shader
        _update_attractors_param(procmat);
        _update_winds_param(procmat);
        _update_vortices_param(procmat);
        _update_floors_param(procmat);

        _procmat_refresh = false;


    }

}

void FutariParticles::_notification(int p_notification) {

    switch (p_notification) {

        case Spatial::NOTIFICATION_ENTER_WORLD:
            //            get_tree()->connect("node_added", this, "_node_added");
            get_tree()->connect("idle_frame", this, "_internal_process");
            break;

        case Spatial::NOTIFICATION_EXIT_WORLD:
            //            get_tree()->disconnect("node_added", this, "_node_added");
            get_tree()->disconnect("idle_frame", this, "_internal_process");
            break;

        case Node::NOTIFICATION_READY:
            break;

        case Node::NOTIFICATION_UNPARENTED:
            break;

    }

    // MainLoop::
    //NOTIFICATION_WM_MOUSE_ENTER = 2,
    //NOTIFICATION_WM_MOUSE_EXIT = 3,
    //NOTIFICATION_WM_FOCUS_IN = 4,
    //NOTIFICATION_WM_FOCUS_OUT = 5,
    //NOTIFICATION_WM_QUIT_REQUEST = 6,
    //NOTIFICATION_WM_GO_BACK_REQUEST = 7,
    //NOTIFICATION_WM_UNFOCUS_REQUEST = 8,
    //NOTIFICATION_OS_MEMORY_WARNING = 9,
    //NOTIFICATION_TRANSLATION_CHANGED = 90,
    //NOTIFICATION_WM_ABOUT = 91,
    //NOTIFICATION_CRASH = 92,
    //NOTIFICATION_OS_IME_UPDATE = 93,

    // Node::
    //NOTIFICATION_ENTER_TREE = 10,
    //NOTIFICATION_EXIT_TREE = 11,
    //NOTIFICATION_MOVED_IN_PARENT = 12,
    //NOTIFICATION_READY = 13,
    //NOTIFICATION_PAUSED = 14,
    //NOTIFICATION_UNPAUSED = 15,
    //NOTIFICATION_PHYSICS_PROCESS = 16,
    //NOTIFICATION_PROCESS = 17,
    //NOTIFICATION_PARENTED = 18,
    //NOTIFICATION_UNPARENTED = 19,
    //NOTIFICATION_INSTANCED = 20,
    //NOTIFICATION_DRAG_BEGIN = 21,
    //NOTIFICATION_DRAG_END = 22,
    //NOTIFICATION_PATH_CHANGED = 23,
    //NOTIFICATION_TRANSLATION_CHANGED = 24,
    //NOTIFICATION_INTERNAL_PROCESS = 25,
    //NOTIFICATION_INTERNAL_PHYSICS_PROCESS = 26,
    //NOTIFICATION_POST_ENTER_TREE = 27,

    // CanvasItem::
    //NOTIFICATION_TRANSFORM_CHANGED = SceneTree::NOTIFICATION_TRANSFORM_CHANGED, //unique
    //NOTIFICATION_DRAW = 30,
    //NOTIFICATION_VISIBILITY_CHANGED = 31,
    //NOTIFICATION_ENTER_CANVAS = 32,
    //NOTIFICATION_EXIT_CANVAS = 33,
    //NOTIFICATION_LOCAL_TRANSFORM_CHANGED = 35,
    //NOTIFICATION_WORLD_2D_CHANGED = 36,

    // Spatial
    //NOTIFICATION_TRANSFORM_CHANGED = SceneTree::NOTIFICATION_TRANSFORM_CHANGED,
    //NOTIFICATION_ENTER_WORLD = 41,
    //NOTIFICATION_EXIT_WORLD = 42,
    //NOTIFICATION_VISIBILITY_CHANGED = 43,
    //NOTIFICATION_LOCAL_TRANSFORM_CHANGED = 44,

};

AABB FutariParticles::get_aabb() const {

    return AABB();
}

PoolVector<Face3> FutariParticles::get_faces(uint32_t p_usage_flags) const {

    return PoolVector<Face3>();
}

void FutariParticles::set_emitting(bool p_emitting) {

    VS::get_singleton()->particles_set_emitting(particles, p_emitting);
}

void FutariParticles::set_amount(int p_amount) {

    ERR_FAIL_COND(p_amount < 1);
    amount = p_amount;
    VS::get_singleton()->particles_set_amount(particles, amount);
}

void FutariParticles::set_lifetime(float p_lifetime) {

    ERR_FAIL_COND(p_lifetime <= 0);
    lifetime = p_lifetime;
    VS::get_singleton()->particles_set_lifetime(particles, lifetime);
}

void FutariParticles::set_one_shot(bool p_one_shot) {

    one_shot = p_one_shot;
    VS::get_singleton()->particles_set_one_shot(particles, one_shot);
    if (!one_shot && is_emitting())
        VisualServer::get_singleton()->particles_restart(particles);
}

void FutariParticles::set_pre_process_time(float p_time) {

    pre_process_time = p_time;
    VS::get_singleton()->particles_set_pre_process_time(particles, pre_process_time);
}

void FutariParticles::set_explosiveness_ratio(float p_ratio) {

    explosiveness_ratio = p_ratio;
    VS::get_singleton()->particles_set_explosiveness_ratio(particles, explosiveness_ratio);
}

void FutariParticles::set_randomness_ratio(float p_ratio) {

    randomness_ratio = p_ratio;
    VS::get_singleton()->particles_set_randomness_ratio(particles, randomness_ratio);
}

void FutariParticles::set_visibility_aabb(const AABB & p_aabb) {

    visibility_aabb = p_aabb;
    VS::get_singleton()->particles_set_custom_aabb(particles, visibility_aabb);
    update_gizmo();
    _change_notify("visibility_aabb");
}

void FutariParticles::set_use_local_coordinates(bool p_enable) {

    local_coords = p_enable;
    VS::get_singleton()->particles_set_use_local_coordinates(particles, local_coords);
}

void FutariParticles::set_process_material(const Ref<Material> &p_material) {

#ifdef TOOLS_ENABLED
    if (process_material.is_valid()) {
        Material* m = *process_material;
        FutariMaterial* fm = Object::cast_to<FutariMaterial>(m);
        if (fm) {
            fm->disconnect("emission_shape_modified", this, "update_gizmo");
        }
    }
#endif

    process_material = p_material;
    RID material_rid;
    if (process_material.is_valid())
        material_rid = process_material->get_rid();
    VS::get_singleton()->particles_set_process_material(particles, material_rid);

    update_configuration_warning();

#ifdef TOOLS_ENABLED
    if (process_material.is_valid()) {
        Material* m = *process_material;
        FutariMaterial* fm = Object::cast_to<FutariMaterial>(m);
        if (fm) {
            fm->connect("emission_shape_modified", this, "update_gizmo");
        }
    }
    update_gizmo();
#endif

}

void FutariParticles::set_speed_scale(float p_scale) {

    speed_scale = p_scale;
    VS::get_singleton()->particles_set_speed_scale(particles, p_scale);
}

bool FutariParticles::is_emitting() const {

    return VS::get_singleton()->particles_get_emitting(particles);
}

int FutariParticles::get_amount() const {

    return amount;
}

float FutariParticles::get_lifetime() const {

    return lifetime;
}

bool FutariParticles::get_one_shot() const {

    return one_shot;
}

float FutariParticles::get_pre_process_time() const {

    return pre_process_time;
}

float FutariParticles::get_explosiveness_ratio() const {

    return explosiveness_ratio;
}

float FutariParticles::get_randomness_ratio() const {

    return randomness_ratio;
}

AABB FutariParticles::get_visibility_aabb() const {

    return visibility_aabb;
}

bool FutariParticles::get_use_local_coordinates() const {

    return local_coords;
}

Ref<Material> FutariParticles::get_process_material() const {

    return process_material;
}

float FutariParticles::get_speed_scale() const {

    return speed_scale;
}

void FutariParticles::set_draw_order(FutariDrawOrder p_order) {

    draw_order = p_order;
    VS::get_singleton()->particles_set_draw_order(particles, VS::ParticlesDrawOrder(p_order));
}

FutariParticles::FutariDrawOrder FutariParticles::get_draw_order() const {

    return draw_order;
}

void FutariParticles::set_draw_passes(int p_count) {

    ERR_FAIL_COND(p_count < 1);
    draw_passes.resize(p_count);
    VS::get_singleton()->particles_set_draw_passes(particles, p_count);
    _change_notify();
}

int FutariParticles::get_draw_passes() const {

    return draw_passes.size();
}

void FutariParticles::set_draw_pass_mesh(int p_pass, const Ref<Mesh> &p_mesh) {

    ERR_FAIL_INDEX(p_pass, draw_passes.size());

    draw_passes.write[p_pass] = p_mesh;

    RID mesh_rid;
    if (p_mesh.is_valid())
        mesh_rid = p_mesh->get_rid();

    VS::get_singleton()->particles_set_draw_pass_mesh(particles, p_pass, mesh_rid);

    update_configuration_warning();
}

Ref<Mesh> FutariParticles::get_draw_pass_mesh(int p_pass) const {

    ERR_FAIL_INDEX_V(p_pass, draw_passes.size(), Ref<Mesh>());

    return draw_passes[p_pass];
}

void FutariParticles::set_fixed_fps(int p_count) {
    fixed_fps = p_count;
    VS::get_singleton()->particles_set_fixed_fps(particles, p_count);
}

int FutariParticles::get_fixed_fps() const {
    return fixed_fps;
}

void FutariParticles::set_fractional_delta(bool p_enable) {
    fractional_delta = p_enable;
    VS::get_singleton()->particles_set_fractional_delta(particles, p_enable);
}

bool FutariParticles::get_fractional_delta() const {
    return fractional_delta;
}

String FutariParticles::get_configuration_warning() const {

    String warnings;

    bool meshes_found = false;
    bool anim_material_found = false;

    for (int i = 0; i < draw_passes.size(); i++) {
        if (draw_passes[i].is_valid()) {
            meshes_found = true;
            for (int j = 0; j < draw_passes[i]->get_surface_count(); j++) {
                anim_material_found = Object::cast_to<ShaderMaterial>(draw_passes[i]->surface_get_material(j).ptr()) != NULL;
                SpatialMaterial *spat = Object::cast_to<SpatialMaterial>(draw_passes[i]->surface_get_material(j).ptr());
                anim_material_found = anim_material_found || (spat && spat->get_billboard_mode() == SpatialMaterial::BILLBOARD_PARTICLES);
            }
            if (meshes_found && anim_material_found) break;
        }
    }

    anim_material_found = anim_material_found || Object::cast_to<ShaderMaterial>(get_material_override().ptr()) != NULL;
    SpatialMaterial *spat = Object::cast_to<SpatialMaterial>(get_material_override().ptr());
    anim_material_found = anim_material_found || (spat && spat->get_billboard_mode() == SpatialMaterial::BILLBOARD_PARTICLES);

    if (!meshes_found) {
        if (warnings != String())
            warnings += "\n";
        warnings += "- " + TTR("Nothing is visible because meshes have not been assigned to draw passes.");
    }

    if (process_material.is_null()) {
        if (warnings != String())
            warnings += "\n";
        warnings += "- " + TTR("A material to process the particles is not assigned, so no behavior is imprinted.");
    } else {
        const FutariMaterial *process = Object::cast_to<FutariMaterial>(process_material.ptr());
        if (!anim_material_found && process &&
                (process->get_param(FutariMaterial::FUPA_ANIM_SPEED) != 0.0 ||
                process->get_param(FutariMaterial::FUPA_ANIM_OFFSET) != 0.0 ||
                process->get_param_texture(FutariMaterial::FUPA_ANIM_SPEED).is_valid() ||
                process->get_param_texture(FutariMaterial::FUPA_ANIM_OFFSET).is_valid())) {
            if (warnings != String())
                warnings += "\n";
            warnings += "- " + TTR("Particles animation requires the usage of a SpatialMaterial with \"Billboard Particles\" enabled.");
        }
    }

    return warnings;
}

void FutariParticles::restart() {

    VisualServer::get_singleton()->particles_restart(particles);
}

AABB FutariParticles::capture_aabb() const {

    return VS::get_singleton()->particles_get_current_aabb(particles);
}

void FutariParticles::_validate_property(PropertyInfo & property) const {

    if (property.name.begins_with("draw_pass_")) {
        int index = property.name.get_slicec('_', 2).to_int() - 1;
        if (index >= draw_passes.size()) {
            property.usage = 0;
            return;
        }
    }
}